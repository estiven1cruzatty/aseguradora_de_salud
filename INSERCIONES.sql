
--table clientes
insert into clientes (id_cliente , id_contrato , id_asesores,cedula_clientes,nombre_cliente,apellido_cliente,tipo_sangre,
fecha_nacimiento ,fecha_ingreso)
values 
('3301','4408','1001','1709338088','PABLO PATRICIO',  'CABRERA PEÑAHERRERA', 'A+','22/12/1995','21/04/2020');
('3302',       '1003','1345345209','ROBERTO MARIN',   'ZAMBRANO ANCHUNDIA',  'A-','16/05/1989','21/04/2016');
('3303','4407','1002','1343348552','MARÍA MARTHA',    'MERO PEREZ',          'A+','11/02/1997','07/03/2014');
('3304',       '1004','170345686' ,'RODRIGO JOSÉ',    'HERRERA MACHUCA',     'A-','27/07/1987','11/04/2015');
('3305','4406','1005','1209345089','CARMEN ELIZABETH','CADENA CALLE',        'O+','02/11/1995','21/04/2020');
('3306','4404','1002','1709338089','PABLO PATRICIO',  'CABRERA PEÑAHERRERA', 'AB+','22/01/1980','31/07/2014');
('3307','4405','1002','1234134563','ROMERO RAMÓN',    'RIVERA CHICA',        'B+','05/06/1979','01/04/2018');
('3308','4403','1001','1378907653','PATRICIA',        'MORÁN LOPÉZ',         'B-','02/02/1981','19/12/2019');
('3309','4402','1005','1232456754','PABLO LUIS',      'CABRERA PEÑAHERRERA', 'B+','02/02/1981','23/04/2017');
('3310','4401','1004','1535312456','JOSÉ LUIS',       'HERRERA ZAMBRANO',    'A+','12/07/1998','30/04/2016');

insert into doctor (id_doctor,tipo_especialidad,nombre_doctor,apellido_doctor,cedula_doctor)
values
('1101','Cirugía General','PABLO MARCELO',          'ABAD NIETO',      '1303753618');
('1102','Radiología',     'BELLA NARCISA DEL PILAR','ABATA REINOSO',   '1706172648');
('1103','Cirugía General','DAVID JOSÉ',             'ACOSTA VÁSQUEZ'   '1714818299');
('1104','Cirugía General','JANETH ALEXANDRA',       'ARIAS MENDOZA',   '802226597');
('1105','Cirugía General','IVÁN PATRICIO',          'ASITIMBAY GUZMÁN','102723178');
('1106','Cirugía General','DAVID ALBERTO',          'ASTUDILLO CELI',  '1103367858');
('1107','Cirugía General','Laura Romina',           'ASTUDILLO CELI',  '1103367858')


--asesores
insert into asesores (id_asesores,cedula_asesor,nombre_asesor,apellido_asesor)
values 
('1001','1309789988','WILLIAM ROBERTO','ZAMBRANO ANCHUNDIA'),
('1002','1323486967','SEBASTIAN ROBERTO','ANCHUNDIA ZAMORA'),
('1003','1355689702','JOSÉ AGUSTÍN','PANCHANA MORA'),
('1004','1234268970','MARÍA ROBERTA','VERÁ PANCHANA'),
('1005','1309642735','PEDRO LUIS','VERÁ COSTA'),

--TABLE CONTRATO_SEGURO
insert into contrato_seguro (id_contrato,tipo_contrato,personas_aseguradas,fecha_contrato)
values 
('4401','FAMILIAR','HERRERA BONILLA GUILLERMO ALBERTO','22/06/2016'),
('4402','FAMILIAR','CABRERA MEDINA CARLOS RICARTE','23/04/2017'),
('4403','FAMILIAR','BRIONES MORÁN EDMUNDO ALBERTO','09/04/2019'),
('4404','FAMILIAR','CABRERA MURILLO RODOLFO ERNESTO','19/12/2021'),
('4405','FAMILIAR','RIVERA ARIAS ANDREA ELIZABETH','02/03/2019'),
('4406','FAMILIAR','CABRERA CADENA CARLOS FERNANDO ALBERTO','22/08/2020'),
('4407','FAMILIAR','CAJAS MERO CINTHIA MARIELA','02/08/2020'),
('4408','FAMILIAR','CABRERA REQUELME GINA MARIZOL','12/07/2020');


--table incremento_anual
insert into incremento_anual (id_incremento,id_contrato,incremento_anual)
values 
('5501','4401','12'),
('5502','4402','12'),
('5503','4403','12'),
('5504','4404','12'),
('5505','4405','12'),
('5506','4406','12'),
('5507','4407','12'),
('5508','4408','12');


--table patologias
insert into patalogias (id_patologia,id_cliente,tipo_patologia,descripcion_patologia)
values 
('2201','3301','Hipertensión arterial','Ataque cardíaco o accidente cerebrovascular'),
('2202','3302','Diabetes melitus','PACIENTE CON DIABETES MELLITUS TIPO 2 CON COMPLICACIONES Y ASOCIADA A 
 REINGRESOS HOSPITALARIOS SEGUIDOS'),
('2203','3303','Trombosis venosa','Coagulos de la sangre en una vena profunda generada en las piernas'),
('2204','3304','Insuficiencia renal','La insuficiencia renal aguda ocurre cuando los riñones pierden de repente la capacidad de filtrar los desechos de la sangre'),
('2205','3305','Asma','Trastorno pulmonal'),
('2206','3306','Insuficiencia cardíaca','se produce cuando el músculo del corazón no bombea sangre tan bien como debería hacerlo'),
('2207','3307','Colelitiasis','depósitos endurecidos de fluido digestivo que se pueden formar en la vesícula biliar'),
('2208','3308','Infección a las vias orinarias','Dolor y ardor al orinar'),
('2209','3309','Asma','Trastorno pulmonal'),
('2210','3310','A.S.V','Derrame cerebral');


--table consulta_medicas
insert into consultas_medicas (id_consulta,id_cliente,consulta_medicas,recetas_medicas_,costo_consulta,costo_receta,propuesta_doctor,fecha_consulta)
values 
('7001','3301','Medicina general','(Advil, Motrin IB cada 24horas) y naproxeno sódico (Aleve).','20','15','Reposo Absoluto','27/02/2020'),
('7002','3308','Cardiología','Carvedidol cada 72 horas y Hidroclorotiazida (Esidrix, Hydrodiuril)','80','900','Implantar dispositivos para regular el
 ritmo cardíaco o mantener la función del corazón y el flujo sanguíneo.(OPERACIÓN)','12/02/2021'),
 
('7003','3309','Psicología','Olanzapina (Zyprexa®)','40','30','Zyprexa se utiliza para tratar la esquizofrenia en adultos.
 La esquizofrenia es una enfermedad mental','11/08/2017'),
('7004','3310','Radiología','radiografías pulmunal.','00','40','Las radiografías del brazo pueden ayudar a determinar la causa 
 de síntomas como dolor, sensibilidad, hinchazón o deformidad en el brazo.','20/04/2018');
 

--TABLE PLANES_SALUD
insert into planes_salud (id_seguro,id_cliente,planes_seguro,estado_seguro,plan_fecha)
values
('9901','3301','PLAN FAMILIAR',   'ACTIVO',  '12/09/2019'),
('9902','3302','PLAN INVIVIDUAL', 'ACTIVO',  '14/04/2020'),
('9903','3303','PLAN EMPRESARIAL','INACTIVO','12/09/2019'),
('9904','3304','PLAN INDIVIDUAL', 'INACTIVO','04/07/2018'),
('9905','3305','PLAN FAMILIAR',   'ACTIVO',  '21/04/2015'),
('9906','3306','PLAN FAMILIAR',   'ACTIVO',  '21/04/2015'),
('9907','3307','PLAN FAMILIAR',   'ACTIVO',  '21/04/2015'),
('9908','3308','PLAN EMPRESARIAL','ACTIVO',  '21/04/2015'),
('9909','3309','PLAN FAMILIAR',   'ACTIVO',  '21/04/2015'),
('9910','3310','PLAN FAMILIAR',   'ACTIVO',  '21/04/2015');


--table factura_seguro
insert into factura_seguro (id_factura,id_cliente,fecha_factura)
values 
('6601','3301','30/06/2021'),
('6602','3302','27/04/2021'),
('6603','3303','28/03/2021'),
('6604','3304','15/03/2021'),
('6605','3305','28/03/2021'),
('6606','3306','29/07/2021'),
('6607','3307','31/05/2021'),
('6608','3308','26/05/2021'),
('6609','3309','30/03/2021'),
('6610','3310','21/04/2021');


--table detalle_factura
insert into detalle_factura (id_detalle,id_factura,valor_consumido,valor_cubierto,valor_total)
values 
('9001','6601','112,50','175,50','288,00'),
('9002','6602','45,75', '200,00','245,00'),
('9003','6603','120,60','200,00','300,60'),
('9004','6604','112,50','360,00','472,50'),
('9005','6605','112,50','57,79','17.029,00');


--TABLE TRATAMIENTO
insert into tratamiento(id_registro,id_cliente,id_doctor,fecha_registro,descripcion_enfermedad,tipode_sangre)
values 
('7701','3301','1106','27/02/2020','FUERTES DELORES DE CABEZA','A+'),
('7702','3308','1105','04/07/2021','OPERACIÓN','A+'),
('7703','3309','1107','11/08/2017','PROBLEMAS MENTALES','A-'),
('7704','3310','1102','20/04/2018','OPERACIÓN','A+'),

--TABLE cotizacion_seguro
insert into cotizacion_seguro(id_cotizacion,id_seguro,tipo_cotizacion,descripcion_cotizacion,valor_cotizacion)
values 
('8801','9901','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50'),
('8802','9902','COTIZACIÓN-PLAN INDIVIDUAL','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS PERSONALES','45,75'),
('8803','9903','COTIZACIÓN-PLAN EMPRESARIAL','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES COMO PERSONALES','120,60'),
('8804','9904','COTIZACIÓN-PLAN INDIVIDUAL','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS PERSONALES','112,50'),
('8805','9905','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50'),
('8806','9906','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50'),
('8807','9907','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50'),
('8808','9908','COTIZACIÓN-PLAN EMPRESARIAL','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES COMO PERSONALES','120,60'),
('8809','9909','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50'),
('8810','9910','COTIZACIÓN-PLAN FAMILIAR','CUENTA CON UN PLAN QUE CUBRE GASTOS MEDICOS FAMILIARES','112,50');