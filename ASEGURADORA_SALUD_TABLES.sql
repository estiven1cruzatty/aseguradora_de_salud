
/*==============================================================*/
/* Table: ASESORES                                              */
/*==============================================================*/
create table ASESORES (
   ID_ASESORES          SERIAL               not null,
   CEDULA_ASESOR        INT4                 null,
   NOMBRE_ASESOR        VARCHAR(40)          null,
   APELLIDO_ASESOR      VARCHAR(40)          null,
   constraint PK_ASESORES primary key (ID_ASESORES)
);


/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES (
   ID_CLIENTE           SERIAL               not null,
   ID_CONTRATO          INT4                 null,
   ID_ASESORES          INT4                 not null,
   CEDULA_CLIENTES      INT                  null,
   NOMBRE_CLIENTE       VARCHAR(40)          null,
   APELLIDO_CLIENTE     VARCHAR(40)          null,
   TIPO_SANGRE          VARCHAR(20)          null,
   FECHA_NACIMIENTO     DATE                 null,
   FECHA_INGRESO        DATE                 null,
   constraint PK_CLIENTES primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Table: CONSULTAS_MEDICAS                                     */
/*==============================================================*/
create table CONSULTAS_MEDICAS (
   ID_CONSULTA          SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   CONSULTA_MEDICA      VARCHAR(100)         null,
   RECETAS_MEDICAS_     VARCHAR(100)         null,
   COSTO_CONSULTA       MONEY                null,
   COSTO_RECETA         MONEY                null,
   PROPUESTA_DOCTOR     VARCHAR(150)         null,
   FECHA_CONSULTA       DATE                 null,
   constraint PK_CONSULTAS_MEDICAS primary key (ID_CONSULTA)
);


/*==============================================================*/
/* Table: CONTRATO_SEGURO                                       */
/*==============================================================*/
create table CONTRATO_SEGURO (
   ID_CONTRATO          SERIAL               not null,
   TIPO_CONTRATO        VARCHAR(40)          null,
   PERSONAS_ASEGURADAS  VARCHAR(50)          null,
   FECHA_CONTRATO       DATE                 null,
   constraint PK_CONTRATO_SEGURO primary key (ID_CONTRATO)
);

/*==============================================================*/
/* Table: COTIZACION_SEGURO                                     */
/*==============================================================*/
create table COTIZACION_SEGURO (
   ID_COTIZACION        SERIAL               not null,
   ID_SEGURO            INT4                 not null,
   TIPO_COTIZACION      VARCHAR(40)          null,
   DESCRIPCION_COTITIZACION VARCHAR(80)          null,
   VALOR_COTIZACION         MONEY                null,
   constraint PK_COTIZACION_SEGURO primary key (ID_COTIZACION)
);
/*==============================================================*/
/* Table: DETALLE_FACTURA                                       */
/*==============================================================*/
create table DETALLE_FACTURA (
   ID_DETALLE           SERIAL               not null,
   ID_FACTURA           INT4                 not null,
   VALOR_CONSUMIDO      MONEY                null,
   VALOR_CUBIERTO       MONEY                null,
   VALOR_TOTAL          MONEY                null,
   constraint PK_DETALLE_FACTURA primary key (ID_DETALLE)
);

/*==============================================================*/
/* Table: DOCTOR                                                */
/*==============================================================*/
create table DOCTOR (
   ID_DOCTOR            SERIAL               not null,
   TIPO_ESPECIALIDAD    VARCHAR(40)          null,
   NOMBRE_DOCTOR        VARCHAR(40)          null,
   APELLIDO_DOCTOR      VARCHAR(40)          null,
   CEDULA_DOCTOR        INT                 null,
   constraint PK_DOCTOR primary key (ID_DOCTOR)
);


/*==============================================================*/
/* Table: FACTURA_SEGURO                                        */
/*==============================================================*/
create table FACTURA_SEGURO (
   ID_FACTURA           SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   FECHA_FACTURA        DATE                 null,
   constraint PK_FACTURA_SEGURO primary key (ID_FACTURA)
);

/*==============================================================*/
/* Table: INCREMENTO_ANUAL                                      */
/*==============================================================*/
create table INCREMENTO_ANUAL (
   ID_INCREMENTO        SERIAL               not null,
   ID_CONTRATO          INT4                 not null,
   INCREMENTO_ANUAL     MONEY                null,
   constraint PK_INCREMENTO_ANUAL primary key (ID_INCREMENTO)
);

/*==============================================================*/
/* Table: PATOLOGIAS                                            */
/*==============================================================*/
create table PATOLOGIAS (
   ID_PATOLOGIA         SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   TIPO_PATOLOGIA       VARCHAR(40)          null,
   DESCRIPCION_PATOLOGIA VARCHAR(150)         null,
   constraint PK_PATOLOGIAS primary key (ID_PATOLOGIA)
);

/*==============================================================*/
/* Table: PLANES_SALUD                                          */
/*==============================================================*/
create table PLANES_SALUD (
   ID_SEGURO            SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   PLANES_SEGURO        VARCHAR(40)          null,
   ESTADO_SEGURO        VARCHAR(40)          null,
   PLAN_FECHA           DATE                 null,
   constraint PK_PLANES_SALUD primary key (ID_SEGURO)
);


/*==============================================================*/
/* Table: SERVICIOS                                             */
/*==============================================================*/
create table SERVICIOS (
   ID_SERVICIO          SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   TIPO_SERVICIO        VARCHAR(40)          null,
   constraint PK_SERVICIOS primary key (ID_SERVICIO)
);
/*==============================================================*/
/* Table: TRATAMIENTO                                           */
/*==============================================================*/
create table TRATAMIENTO (
   ID_REGISTRO          SERIAL               not null,
   ID_CLIENTE           INT4                 not null,
   ID_DOCTOR            INT4                 not null,
   FECHA_REGISTRO       DATE                 null,
   DESCRIPCION_ENFERMEDAD VARCHAR(60)          null,
   TIPODE_SANGRE        VARCHAR(20)          null,
   constraint PK_TRATAMIENTO primary key (ID_REGISTRO)
);


alter table CLIENTES
   add constraint FK_CLIENTES_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO_SEGURO (ID_CONTRATO)
      on delete restrict on update restrict;

alter table CLIENTES
   add constraint FK_CLIENTES_RELATIONS_ASESORES foreign key (ID_ASESORES)
      references ASESORES (ID_ASESORES)
      on delete restrict on update restrict;

alter table CONSULTAS_MEDICAS
   add constraint FK_CONSULTA_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table COTIZACION_SEGURO
   add constraint FK_COTIZACI_RELATIONS_PLANES_S foreign key (ID_SEGURO)
      references PLANES_SALUD (ID_SEGURO)
      on delete restrict on update restrict;

alter table DETALLE_FACTURA
   add constraint FK_DETALLE__RELATIONS_FACTURA_ foreign key (ID_FACTURA)
      references FACTURA_SEGURO (ID_FACTURA)
      on delete restrict on update restrict;

alter table FACTURA_SEGURO
   add constraint FK_FACTURA__RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table INCREMENTO_ANUAL
   add constraint FK_INCREMEN_RELATIONS_CONTRATO foreign key (ID_CONTRATO)
      references CONTRATO_SEGURO (ID_CONTRATO)
      on delete restrict on update restrict;

alter table PATOLOGIAS
   add constraint FK_PATOLOGI_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table PLANES_SALUD
   add constraint FK_PLANES_S_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table SERVICIOS
   add constraint FK_SERVICIO_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table TRATAMIENTO
   add constraint FK_TRATAMIE_RELATIONS_CLIENTES foreign key (ID_CLIENTE)
      references CLIENTES (ID_CLIENTE)
      on delete restrict on update restrict;

alter table TRATAMIENTO
   add constraint FK_TRATAMIE_RELATIONS_DOCTOR foreign key (ID_DOCTOR)
      references DOCTOR (ID_DOCTOR)
      on delete restrict on update restrict;
